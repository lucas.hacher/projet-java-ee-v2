<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="ENI Ecole">
<meta name="author" content="ENI Ecole">


<!-- Bootstrap core CSS -->
<link
	href="${pageContext.request.contextPath}/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">

<!-- Custom styles for this template -->
<link href="${pageContext.request.contextPath}/css/4-col-portfolio.css"
	rel="stylesheet">
<link rel="icon"
	href="${pageContext.request.contextPath}/images/favicon.ico">

<link href="${pageContext.request.contextPath}/css/style.css"
	rel="stylesheet">
<title>Ajout</title>
</head>
<body>

	<!-- Page Content -->
	<div class="container">

		<!-- Page Heading -->
		<h1 class="my-4">Nouvel utilisateur</h1>

		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 portfolio-item">
				<div class="card h-100">
					<div class="card-body contenu">
						<div class="contenu">

							<c:if test="${!empty erreur}">
								<div class="alert alert-danger" role="alert">
									<strong>Erreur!</strong>
									${erreur}
								</div>
							</c:if>

							<form action="${pageContext.request.contextPath}/ajout"
								method="post" class="justify-content-center mb-2">
								<div class="saisie">
									<label for="nom">Nom : </label> <input class="form-control"
										type="text" name="nom" value="${nom}" />
								</div>
								<div class="saisie">
									<label for="prenom">Prénom : </label> <input class="form-control"
										type="text" name="prenom" value="${prenom}" />
								</div>
								<div class="saisie">
									<label for="email">Email : </label>
									<input  class="form-control" name="email" value="${email}">
								</div>
								<div class="saisie">
									<label for="password">Password : </label>
									<input type="password" class="form-control" name="password" value="${password}">
								</div>

								<div>
									<input type="submit" value="Valider" class="btn btn-primary" />
								</div>
							</form>

						</div>
					</div>
				</div>
			</div>

		</div>
		<!-- /.row -->

	</div>
	<!-- /.container -->



	<!-- Bootstrap core JavaScript -->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>