<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib tagdir="/WEB-INF/tags/" prefix="t" %>

    <t:layout>
        <div class="card-panel">
            <div class="container">

				<!-- Page Heading -->
				<h1 class="my-4">Connexion</h1>

				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 portfolio-item">
						<div class="h-100">
								<div class="contenu">

									<c:if test="${!empty erreur}">
										<div class="alert alert-danger" role="alert">
											<strong>Erreur!</strong> ${erreur}
										</div>
									</c:if>
                                    <c:if test="${ !empty sessionScope.pseudo && !empty sessionScope.password}">
                                    <p>Vous êtes ${ sessionScope.password} !</p>
                                    </c:if>

									<form action="${pageContext.request.contextPath}/login"
										method="post" class="justify-content-center mb-2">
										<div class="saisie">
											<label for="pseudo">Pseudo : </label>
											<input class="form-control" type="pseudo"
												required name="pseudo" value="${pseudo}">
										</div>
										<div class="saisie">
											<label for="password">Password : </label> <input
												type="password" class="form-control" required name="password"
												value="${password}">
										</div>

										<div>
											<input type="submit" value="Valider" class="btn btn-primary" />
										</div>
										<div>
											<a href="${pageContext.request.contextPath}/inscription">S'inscrire</a>
										</div>
									</form>

								</div>
						</div>
					</div>

				</div>

			</div>
        </div>
    </t:layout>

