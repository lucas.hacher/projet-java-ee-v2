<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib tagdir="/WEB-INF/tags/" prefix="t" %>

<t:layout>
	<div class="container">

		<!-- Page Heading -->
		<h1 class="my-4">Profil</h1>

		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 portfolio-item">
				<div class="card h-100">
					<div class="card-body contenu">
						<div class="contenu">

							<c:if test="${!empty erreur}">
								<div class="alert alert-danger" role="alert">
									<strong>Erreur!</strong> ${erreur}
								</div>
							</c:if>


							<form action="${pageContext.request.contextPath}/profil"
                            								method="post" class="justify-content-center mb-2">
							  <div class="form-group">
                                 <label for="pseudo">Pseudo:</label>
                                 <input type="pseudo" class="form-control" placeholder="Entrer le pseudo" id="pseudo"
                                 required name="pseudo" value=${sessionScope.pseudo}>
                              </div>
                              <div class="form-group">
                                 <label for="nom">Nom:</label>
                                 <input type="nom" class="form-control" placeholder="Entrer le nom" id="nom"
                                 required name="nom" value=${utilisateur.nom}>
                              </div>
                              <div class="form-group">
                                 <label for="prenom">Prenom:</label>
                                 <input type="prenom" class="form-control" placeholder="Entrer le prénom" id="prenom"
                                 required name="prenom" value=${utilisateur.prenom}>
                              </div>
                              <div class="form-group">
                                <label for="email">Email address:</label>
                                <input type="email" class="form-control" placeholder="Entrer l'email" id="email"
                                required name="email" value=${utilisateur.email}>
                              </div>
                              <div class="form-group">
                                 <label for="tel">Téléphone:</label>
                                 <input type="telephone" class="form-control" placeholder="Entrer le téléphone"
                                 pattern="[0-9]{2}[0-9]{2}[0-9]{2}[0-9]{2}[0-9]{2}"id="telephone"
                                 required name="telephone" value=${utilisateur.telephone}>
                              </div>
                              <div class="form-group">
                                 <label for="rue">Rue:</label>
                                 <input type="rue" class="form-control" placeholder="Entrer la rue" id="rue"
                                 required name="rue" value=${utilisateur.rue}>
                              </div>
                              <div class="form-group">
                                <label for="cp">Code postal:</label>
                                <input type="cp" class="form-control" placeholder="Entrer le code postal" id="cp"
                                pattern="[0-9]{5}" required name="cp" value=${utilisateur.cp} >
                              </div>
                              <div class="form-group">
                                <label for="ville">Ville:</label>
                                <input type="ville" class="form-control" placeholder="Entrer la ville" id="ville"
                                required name="ville" value=${utilisateur.ville}>
                              </div>





                              <button  name="valid" value="update" class="btn btn-primary">Modifier</button>
                              <button  name="valid" value="delete" class="btn btn-primary">Supprimer</button>
                            </form>

						</div>
					</div>
				</div>
			</div>

		</div>
		<!-- /.row -->

	</div>
</t:layout>


