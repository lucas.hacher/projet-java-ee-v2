<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib tagdir="/WEB-INF/tags/" prefix="t" %>
<%@page import="java.util.List"%>
<%@page import="fr.eni.enchere.bo.ArticleVendu"%>
<%@page import="fr.eni.enchere.bo.Categorie"%>


<t:layout>
    <div class="card-panel">
        <form action="${pageContext.request.contextPath}/listeEnchere" method="post">
            <div class="row">
                <h4>Liste des enchères</h4>
            </div>

            <br />

            <div>
                <h5>Filtres :</h5>

                <br />

                <%-- Radio Button Achat et Vente --%>

                <c:if test="${ !empty sessionScope.pseudo && !empty sessionScope.password}">

                    <div class="group-block">
                            <div class="solo-block">
                                <p>
                                    <label>
                                        <input name="achatRadio" type="radio" id="achatRadio" onclick="displayAchat();" checked />
                                        <span>Achats</span>
                                    </label>
                                </p>
                            </div>
                            <div class="solo-block">
                                <p>
                                    <label>
                                        <input name="venteRadio" type="radio" id="venteRadio" onclick="displayVente();" />
                                        <span>Mes ventes</span>
                                    </label>
                                </p>
                            </div>
                    </div>

                </c:if>


                <%-- Select Categorie --%>

                <div class="group-block categorie-search">

                    <c:choose>
                        <c:when test="${categorieList != null}">

                                    <div class="solo-block">
                                        <div id="inner">
                                            <div class="input-field col s12 child">
                                                <select id="categorieChoice" name="categorieChoice">

                                                        <option value="" disabled selected>Tout</option>

                                                    <c:forEach var="categorie" items="${categorieList}">

                                                        <option value="${categorie.no_categorie}">${categorie.libelle}</option>

                                                    </c:forEach>

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                
                        </c:when>

                        <c:otherwise>
                            <div class="solo-block">
                                <div id="inner">
                                    <div class="input-field col s12 child">
                                        <select>
                                            <option value="" disabled selected>Catégories</option>
                                            <option value="" disabled>Aucune catégorie disponible</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </c:otherwise>
                    </c:choose>


                    <%-- Barre de recherche --%>

                    <div class="solo-block search">
                        <nav class="orange accent-4">
                            <div class="nav-wrapper">
                                    <div class="input-field">
                                        <input id="search" type="search" name="search">
                                        <label class="label-icon" for="search"><i
                                                class="material-icons">search</i></label>
                                        <i class="material-icons">close</i>
                                    </div>
                            </div>
                        </nav>
                    </div>
                </div>

                <br />

                <%-- Checkbox Achat --%>

                <c:if test="${ !empty sessionScope.pseudo && !empty sessionScope.password}">

                <div class="group-block" id="achatCheckbox">
                    <form action="#">
                        <div class="solo-block">
                            <p>
                                <label>
                                    <input type="checkbox" class="filled-in" id="enchereOuverte"/>
                                    <span>Enchères ouvertes</span>
                                </label>
                            </p>
                        </div>
                        <div class="solo-block">
                            <p>
                                <label>
                                    <input type="checkbox" class="filled-in" id="enchereEnCours" />
                                    <span>Mes enchères en cours</span>
                                </label>
                            </p>
                        </div>
                        <div class="solo-block">
                            <p>
                                <label>
                                    <input type="checkbox" class="filled-in" id="enchereRemporte"/>
                                    <span>Mes enchères remportées</span>
                                </label>
                            </p>
                        </div>
                    </form>
                </div>

                <%-- Checkbox Vente --%>

                <div class="group-block" id="venteCheckbox" style="display:none;">
                        <div class="solo-block">
                            <p>
                                <label>
                                    <input type="checkbox" class="filled-in" id="venteEnCours" disabled="disabled"/>
                                    <span>Mes ventes en cours</span>
                                </label>
                            </p>
                        </div>
                        <div class="solo-block">
                            <p>
                                <label>
                                    <input type="checkbox" class="filled-in" id="venteNonDebute" disabled="disabled"/>
                                    <span>Ventes non débutées</span>
                                </label>
                            </p>
                        </div>
                        <div class="solo-block">
                            <p>
                                <label>
                                    <input type="checkbox" class="filled-in" id="venteTermine" disabled="disabled"/>
                                    <span>Ventes terminées</span>
                                </label>
                            </p>
                        </div>
                </div>

                </c:if>

                <%-- Submit Button --%>

                <div class="center recherche">
                    <input class="btn-small blue accent-2 displayResults" value="Rechercher" type="submit" />
                </div>

            </div>

        
            
            
            </div>
        </form>

    </div>




    <%-- Liste des Articles --%>

    <div class="container" id="results">

        <h3 id="titleSearch">Résultat pour la recherche :</h3>

        <c:choose>

            <c:when test="${articleList != null || articleList.size() > 0}">
                <div class="list-group">

                    <c:forEach var="article" items="${articleList}">

                        <div class="group-block">
                            <c:choose>
                                <c:when test="${ !empty sessionScope.pseudo && !empty sessionScope.password}">
                                    <a href="${pageContext.request.contextPath}/ref?idArticle=${article.no_article}" class="list-group-item list-group-item-action flex-column align-items-start">
                                </c:when>    
                                <c:otherwise>
                                    <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                                </c:otherwise>
                            </c:choose>
                                <div class="solo-block picture">
                                    <img class="card-img-left" src="${pageContext.request.contextPath}/img/no-image-found.png" alt="Card image cap">
                                </div>
                                <div class="solo-block article">
                                    <div class="d-flex w-100 justify-content-between">
                                        <h5 class="mb-1">${article.nom_article}</h5>
                                    </div>
                                    <p class="mb-1">Prix : ${article.prix_vente}€</p>
                                    <p class="mb-1">Fin de l'enchère : ${article.date_fin_encheres}</p>
                                    <p class="mb-1">Vendeur : ${article.nom_article}</p>
                                </div>
                            </a>
                        </div>

                    </c:forEach>

                </div>
            </c:when>

            <c:when test="${listArticleByCategorie != null || listArticleByCategorie.size() > 0}">
                <div class="list-group">

                    <c:forEach var="article" items="${listArticleByCategorie}">

                        <div class="group-block">
                                <c:choose>
                                    <c:when test="${ !empty sessionScope.pseudo && !empty sessionScope.password}">
                                        <a href="${pageContext.request.contextPath}/ref?idArticle=${article.no_article}" class="list-group-item list-group-item-action flex-column align-items-start">
                                    </c:when>    
                                    <c:otherwise>
                                        <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                                    </c:otherwise>
                                </c:choose>
                                <div class="solo-block picture">
                                    <img class="card-img-left" src="${pageContext.request.contextPath}/img/no-image-found.png" alt="Card image cap">
                                </div>
                                <div class="solo-block article">
                                    <div class="d-flex w-100 justify-content-between">
                                        <h5 class="mb-1">${article.nom_article}</h5>
                                    </div>
                                    <p class="mb-1">Prix : ${article.prix_vente}€</p>
                                    <p class="mb-1">Fin de l'enchère : ${article.date_fin_encheres}</p>
                                    <p class="mb-1">Vendeur : ${article.nom_article}</p>
                                </div>
                            </a>
                        </div>

                    </c:forEach>

                </div>
            </c:when>

            <c:when test="${listArticleByMotCle != null || listArticleByMotCle.size() > 0}">
                <div class="list-group">

                    <c:forEach var="article" items="${listArticleByMotCle}">

                        <div class="group-block">
                                <c:choose>
                                    <c:when test="${ !empty sessionScope.pseudo && !empty sessionScope.password}">
                                        <a href="${pageContext.request.contextPath}/ref?idArticle=${article.no_article}" class="list-group-item list-group-item-action flex-column align-items-start">
                                    </c:when>    
                                    <c:otherwise>
                                        <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                                    </c:otherwise>
                                </c:choose>
                                <div class="solo-block picture">
                                    <img class="card-img-left" src="${pageContext.request.contextPath}/img/no-image-found.png" alt="Card image cap">
                                </div>
                                <div class="solo-block article">
                                    <div class="d-flex w-100 justify-content-between">
                                        <h5 class="mb-1">${article.nom_article}</h5>
                                    </div>
                                    <p class="mb-1">Prix : ${article.prix_vente}€</p>
                                    <p class="mb-1">Fin de l'enchère : ${article.date_fin_encheres}</p>
                                    <p class="mb-1">Vendeur : ${article.nom_article}</p>
                                </div>
                            </a>
                        </div>

                    </c:forEach>

                </div>
            </c:when>

            <c:when test="${listArticleByMotCleAndByCategorie != null || listArticleByMotCleAndByCategorie.size() > 0}">
                <div class="list-group">

                    <c:forEach var="article" items="${listArticleByMotCleAndByCategorie}">

                        <div class="group-block">
                                <c:choose>
                                    <c:when test="${ !empty sessionScope.pseudo && !empty sessionScope.password}">
                                        <a href="${pageContext.request.contextPath}/ref?idArticle=${article.no_article}" class="list-group-item list-group-item-action flex-column align-items-start">
                                    </c:when>    
                                    <c:otherwise>
                                        <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                                    </c:otherwise>
                                </c:choose>
                                <div class="solo-block picture">
                                    <img class="card-img-left" src="${pageContext.request.contextPath}/img/no-image-found.png" alt="Card image cap">
                                </div>
                                <div class="solo-block article">
                                    <div class="d-flex w-100 justify-content-between">
                                        <h5 class="mb-1">${article.nom_article}</h5>
                                    </div>
                                    <p class="mb-1">Prix : ${article.prix_vente}€</p>
                                    <p class="mb-1">Fin de l'enchère : ${article.date_fin_encheres}</p>
                                    <p class="mb-1">Vendeur : ${article.nom_article}</p>
                                </div>
                            </a>
                        </div>

                    </c:forEach>

                </div>
            </c:when>

            <c:otherwise>
                <div>
                    <h3>Aucun résultat pour votre recherche</h3>
                </div>
            </c:otherwise>
        </c:choose>



    </div>


    <script src="${pageContext.request.contextPath}/js/displayCheckbox.js" type="text/javascript"></script>
    <script>
        
        document.addEventListener('DOMContentLoaded', function () {
            var elems = document.querySelectorAll('select');
            var instances = M.FormSelect.init(elems);
        });

    </script>

</t:layout>