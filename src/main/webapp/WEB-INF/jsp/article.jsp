<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib tagdir="/WEB-INF/tags/" prefix="t" %>
<%@page import="java.util.List"%>
<%@page import="fr.eni.enchere.bo.ArticleVendu"%>
<%@page import="fr.eni.enchere.bo.Categorie"%>


<t:layout>
    <div class="card-panel">

            <div class="row groupCard">
                <div class="col s12 m6">
                    <div class="card">
                        <div class="card-image">
                        <img src="${pageContext.request.contextPath}/img/no-image-found.png">
                        </div>
                        <div class="card-content">
                        <p>${article.description}</p>
                        </div>
                    </div>
                </div>

                <div class="col s12 m6">
                    <br>
                    <p class="caption">
                        <h5 class="card-title titleCard">${article.nom_article}</h5>
                        <h5 class="card-title">Catégorie :</h5>
                        <h5 class="card-title">Meilleure offre :</h5>
                        <h5 class="card-title">Mise à prix :</h5>
                        <h5 class="card-title">Fin de l'enchère : ${article.date_fin_encheres}</h5>
                        <h5 class="card-title">Retrait :</h5>
                        <h5 class="card-title">Ma proposition :</h5>
                    </p>
                </div>

                <div class="col s12 m6 sellerCard">
                    <div class="card blue-grey darken-1">
                        <div class="card-content white-text card-height">
                            <i class="medium material-icons">person</i>
                            <span class="card-title">Infos sur le vendeur :</span>
                            <span class="card-title">Nom du vendeur</span>
                        </div>
                        <div class="card-action">
                            <a href="#">Voir les autres articles de ce vendeur</a>
                        </div>
                    </div>
                </div>

            </div>

    </div>


</t:layout>