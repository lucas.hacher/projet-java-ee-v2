<%@tag description="navbar" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<header>
    <nav class="orange accent-4" role="navigation">
        <div class="nav-wrapper container">

            <a href="./listeEnchere" class="brand-logo link-navbar">Nice Enchere</a>

           <c:choose>
            <c:when test="${ !empty sessionScope.pseudo && !empty sessionScope.password}">


            <ul class="right hide-on-med-and-down">
                <li><a href="./listeEnchere" class="link-navbar"><i class="material-icons right">library_books</i> Enchères</a></li>
                <li><a href="#" class="link-navbar"><i class="material-icons right">attach_money</i>Vendre un article</a></li>
                <li><a href="./profil" class="link-navbar"><i class="material-icons right">person</i>Bonjour ${sessionScope.pseudo} </a></li>
                <li><a href="./logout" class="link-navbar"><i class="material-icons right">exit_to_app</i>Déconnexion</a></li>
            </ul>
            </c:when>
            <c:otherwise>
            <ul class="right hide-on-med-and-down">
                <li><a href="./listeEnchere" class="link-navbar"><i class="material-icons right">library_books</i>Enchères</a></li>
                <li><a href="./login" class="link-navbar"><i class="material-icons right">exit_to_app</i>Connexion</a></li>
            </ul>
            </c:otherwise>
            </c:choose>

            <c:choose>
                        <c:when test="${ !empty sessionScope.pseudo && !empty sessionScope.password}">
            <ul id="nav-mobile" class="sidenav">
                <li class="active"><a href="./listeEnchere" class="link-navbar-mobile"><i class="material-icons left">attach_money</i> Vendre un article</a></li>
                <li><a href="./listeEnchere" class="link-navbar-mobile"><i class="material-icons left">library_books</i> Enchères</a></li>
                <li><a href="./profil" class="link-navbar-mobile"><i class="material-icons left">person</i>Bonjour${sessionScope.pseudo} </a></li>
                <li><a href="./logout" class="link-navbar-mobile"><i class="material-icons left">exit_to_app</i> Déconnexion</a></li>
            </ul>

            <a href="#" data-target="nav-mobile" class="sidenav-trigger link-navbar"><i class="material-icons">menu</i></a>
            </c:when>
            <c:otherwise>
            <ul id="nav-mobile" class="sidenav">
                <li><a href="./listeEnchere" class="link-navbar-mobile"><i class="material-icons left">library_books</i>Enchères</a></li>
                <li><a href="./login" class="link-navbar-mobile"><i class="material-icons left">exit_to_app</i>Se connecter</a></li>
            </ul>

            <a href="#" data-target="nav-mobile" class="sidenav-trigger link-navbar"><i class="material-icons">menu</i></a>
            </c:otherwise>
            </c:choose>
        </div>
    </nav>

</header>