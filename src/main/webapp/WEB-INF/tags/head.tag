<%@tag description="head" pageEncoding="UTF-8"%>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0" />
    <title>${pageTitle}</title>


    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!--Import materialize.css-->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/vendor/materialize/css/materialize.css" />

    <!--Import bootstrap.min.css-->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/vendor/bootstrap/css/bootstrap.min.css" />

    <!--Import Custom CSS-->
    <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet">

    <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/favicon.ico"/>
    <link rel="icon" href="${pageContext.request.contextPath}/img/favicon.ico"/>

</head>