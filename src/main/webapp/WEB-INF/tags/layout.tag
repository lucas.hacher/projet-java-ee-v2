<%@tag description="layout" pageEncoding="UTF-8"%>
<%@taglib tagdir="/WEB-INF/tags/" prefix="t" %>
<!DOCTYPE html>
<html lang="en">

    <t:head />

    <body class="grey lighten-5">

        <t:navbar />

        <main class="container">
            <jsp:doBody />
        </main>

        <!--  Scripts-->
        <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
        <script src="${pageContext.request.contextPath}/vendor/materialize/js/materialize.js"></script>
        <script>
            $(function () {
                $('.sidenav').sidenav();
            });
        </script>

    </body>

</html>