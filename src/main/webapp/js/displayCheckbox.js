var achatRadio = document.getElementById('achatRadio');
var venteRadio = document.getElementById('venteRadio');

var achatCheckbox = document.getElementById('achatCheckbox');
var venteCheckbox = document.getElementById('venteCheckbox');

var enchereOuverte = document.getElementById('enchereOuverte');
var enchereEnCours = document.getElementById('enchereEnCours');
var enchereRemporte = document.getElementById('enchereRemporte');
var venteEnCours = document.getElementById('venteEnCours');
var venteNonDebute = document.getElementById('venteNonDebute');
var venteTermine = document.getElementById('venteTermine');

document.addEventListener('DOMContentLoaded', function () {



    if (achatRadio.checked) {
        achatCheckbox.style.display = 'block';
        venteCheckbox.style.display = 'none';
    } else if (venteRadio.checked) {
        venteCheckbox.style.display = 'block';
        achatCheckbox.style.display = 'none';
    }


});

function displayAchat() {
    venteRadio.checked = false;

    achatCheckbox.style.display = 'block';
    venteCheckbox.style.display = 'none';

    enchereOuverte.removeAttribute('disabled');
    enchereEnCours.removeAttribute('disabled');
    enchereRemporte.removeAttribute('disabled');
    venteEnCours.setAttribute('disabled', 'disabled');
    venteNonDebute.setAttribute('disabled', 'disabled');
    venteTermine.setAttribute('disabled', 'disabled');

    venteEnCours.removeAttribute('checked');
    venteNonDebute.removeAttribute('checked');
    venteTermine.removeAttribute('checked');

    if (venteEnCours.checked || venteNonDebute.checked || venteTermine.checked) {
        venteEnCours.checked = false;
        venteNonDebute.checked = false;
        venteTermine.checked = false;
    }
}

function displayVente() {
    achatRadio.checked = false;

    venteCheckbox.style.display = 'block';
    achatCheckbox.style.display = 'none';

    enchereOuverte.setAttribute('disabled', 'disabled');
    enchereEnCours.setAttribute('disabled', 'disabled');
    enchereRemporte.setAttribute('disabled', 'disabled');
    venteEnCours.removeAttribute('disabled');
    venteNonDebute.removeAttribute('disabled');
    venteTermine.removeAttribute('disabled');

    if (enchereOuverte.checked || enchereEnCours.checked || enchereRemporte.checked) {
        enchereOuverte.checked = false;
        enchereEnCours.checked = false;
        enchereRemporte.checked = false;
    }
}