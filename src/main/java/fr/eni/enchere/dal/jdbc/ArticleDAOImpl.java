package fr.eni.enchere.dal.jdbc;

import fr.eni.enchere.bo.ArticleVendu;
import fr.eni.enchere.bo.Categorie;
import fr.eni.enchere.bo.Utilisateur;
import fr.eni.enchere.dal.ArticleDAO;
import fr.eni.enchere.dal.DALException;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ArticleDAOImpl implements ArticleDAO {

    private static final String sqlSelectById = "select * from ARTICLES_VENDUS where no_article = ?";
    private static final String sqlSelectAllArticle = "select * from ARTICLES_VENDUS";
    private static final String sqlSelectByCategorie = "select * from ARTICLES_VENDUS where no_categorie = ?";
    private static final String sqlSelectByMotCle = "select * from ARTICLES_VENDUS where nom_article like concat ('%', ?, '%')";
    private static final String sqlSelectByMotCleAndByCategorie = "select * " +
            " from ARTICLES_VENDUS where nom_article like concat ('%', ?, '%') and no_categorie = ?";
    private static final String sqlSelectAllCategorie = "select no_categorie, libelle " +
            " from CATEGORIES";


    @Override
    public ArticleVendu selectById(int id) throws DALException {
        Connection cnx = null;
        PreparedStatement rqt = null;
        ResultSet rs = null;
        ArticleVendu articleVendu = new ArticleVendu();
        try {
            cnx = JdbcTools.getConnection();
            rqt = cnx.prepareStatement(sqlSelectById);
            rqt.setInt(1, id);

            rs = rqt.executeQuery();
            if (rs.next()){

                articleVendu.setNo_article(rs.getInt("no_article"));
                articleVendu.setNom_article(rs.getString("nom_article"));
                articleVendu.setDescription(rs.getString("description"));
                articleVendu.setDate_debut_encheres(rs.getDate("date_debut_encheres"));
                articleVendu.setDate_fin_encheres(rs.getDate("date_fin_encheres"));
                articleVendu.setPrix_initial(rs.getInt("prix_initial"));
                articleVendu.setPrix_vente(rs.getInt("prix_vente"));
                articleVendu.getCategorie().setNo_categorie(rs.getInt("no_categorie"));
                articleVendu.getUtilisateur().setNo_utilisateurs(rs.getInt("no_utilisateur"));


            }

        } catch (SQLException e) {
            throw new DALException("selectById failed - id = " + id , e);
        } finally {
            try {
                if (rs != null){
                    rs.close();
                }
                if (rqt != null){
                    rqt.close();
                }
                if(cnx!=null){
                    cnx.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        return articleVendu;
    }

    @Override
    public List<ArticleVendu> selectAllArticle() throws DALException {
        Connection cnx = null;
        Statement rqt = null;
        ResultSet rs = null;
        List<ArticleVendu> list = new ArrayList<ArticleVendu>();
        try {
            cnx = JdbcTools.getConnection();
            rqt = cnx.createStatement();
            rs = rqt.executeQuery(sqlSelectAllArticle);

            while (rs.next()) {

                    ArticleVendu articleVendu = new ArticleVendu();
                    articleVendu.setNo_article(rs.getInt("no_article"));
                    articleVendu.setNom_article(rs.getString("nom_article"));
                    articleVendu.setDescription(rs.getString("description"));
                    articleVendu.setDate_debut_encheres(rs.getDate("date_debut_encheres"));
                    articleVendu.setDate_fin_encheres(rs.getDate("date_fin_encheres"));
                    articleVendu.setPrix_initial(rs.getInt("prix_initial"));
                    articleVendu.setPrix_vente(rs.getInt("prix_vente"));
                    articleVendu.getUtilisateur().setNo_utilisateurs(rs.getInt("no_utilisateur"));
                    articleVendu.getCategorie().setNo_categorie(rs.getInt("no_categorie"));
                    list.add(articleVendu);
            }
        } catch (SQLException e) {
            throw new DALException("selectAll failed - " , e);
        } finally {
            try {
                if (rs != null){
                    rs.close();
                }
                if (rqt != null){
                    rqt.close();
                }
                if(cnx!=null){
                    cnx.close();
                }
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return list;
    }

    @Override
    public List<ArticleVendu> selectByCategorie(String categorie) throws DALException {
        Connection cnx = null;
        PreparedStatement rqt = null;
        ResultSet rs = null;
        List<ArticleVendu> list = new ArrayList<ArticleVendu>();
        try {
            cnx = JdbcTools.getConnection();
            rqt = cnx.prepareStatement(sqlSelectByCategorie);
            rqt.setString(1, categorie);
            rs = rqt.executeQuery();
            ArticleVendu art = null;

            while (rs.next()) {

                ArticleVendu articleVendu = new ArticleVendu();
                articleVendu.setNo_article(rs.getInt("no_article"));
                articleVendu.setNom_article(rs.getString("nom_article"));
                articleVendu.setDescription(rs.getString("description"));
                articleVendu.setDate_debut_encheres(rs.getDate("date_debut_encheres"));
                articleVendu.setDate_fin_encheres(rs.getDate("date_fin_encheres"));
                articleVendu.setPrix_initial(rs.getInt("prix_initial"));
                articleVendu.setPrix_vente(rs.getInt("prix_vente"));
                articleVendu.getUtilisateur().setNo_utilisateurs(rs.getInt("no_utilisateur"));
                articleVendu.getCategorie().setNo_categorie(rs.getInt("no_categorie"));
                list.add(articleVendu);
            }
        } catch (SQLException e) {
            throw new DALException("selectByCategorie failed - " , e);
        } finally {
            try {
                if (rs != null){
                    rs.close();
                }
                if (rqt != null){
                    rqt.close();
                }
                if(cnx!=null){
                    cnx.close();
                }
            } catch (SQLException e) {
                throw new DALException("close failed " , e);
            }
        }
        return list;
    }

    @Override
    public List<ArticleVendu> selectByMotCle(String motCle) throws DALException {
        Connection cnx = null;
        PreparedStatement rqt = null;
        ResultSet rs = null;
        List<ArticleVendu> list = new ArrayList<ArticleVendu>();
        try {
            cnx = JdbcTools.getConnection();
            rqt = cnx.prepareStatement(sqlSelectByMotCle);
            rqt.setString(1, motCle);
            rs = rqt.executeQuery();
            ArticleVendu art = null;

            while (rs.next()) {

                ArticleVendu articleVendu = new ArticleVendu();
                articleVendu.setNo_article(rs.getInt("no_article"));
                articleVendu.setNom_article(rs.getString("nom_article"));
                articleVendu.setDescription(rs.getString("description"));
                articleVendu.setDate_debut_encheres(rs.getDate("date_debut_encheres"));
                articleVendu.setDate_fin_encheres(rs.getDate("date_fin_encheres"));
                articleVendu.setPrix_initial(rs.getInt("prix_initial"));
                articleVendu.setPrix_vente(rs.getInt("prix_vente"));
                articleVendu.getUtilisateur().setNo_utilisateurs(rs.getInt("no_utilisateur"));
                articleVendu.getCategorie().setNo_categorie(rs.getInt("no_categorie"));
                list.add(articleVendu);
            }
        } catch (SQLException e) {
            throw new DALException("selectByMotCle failed - " , e);
        } finally {
            try {
                if (rs != null){
                    rs.close();
                }
                if (rqt != null){
                    rqt.close();
                }
                if(cnx!=null){
                    cnx.close();
                }
            } catch (SQLException e) {
                throw new DALException("close failed " , e);
            }
        }
        return list;
    }

    @Override
    public List<ArticleVendu> selectByMotCleAndByCategorie(String motCle, String categorie) throws DALException {
        Connection cnx = null;
        PreparedStatement rqt = null;
        ResultSet rs = null;
        List<ArticleVendu> list = new ArrayList<ArticleVendu>();
        try {
            cnx = JdbcTools.getConnection();
            rqt = cnx.prepareStatement(sqlSelectByMotCleAndByCategorie);
            rqt.setString(1, motCle);
            rqt.setString(2, categorie);
            rs = rqt.executeQuery();
            ArticleVendu art = null;

            while (rs.next()) {

                ArticleVendu articleVendu = new ArticleVendu();
                articleVendu.setNo_article(rs.getInt("no_article"));
                articleVendu.setNom_article(rs.getString("nom_article"));
                articleVendu.setDescription(rs.getString("description"));
                articleVendu.setDate_debut_encheres(rs.getDate("date_debut_encheres"));
                articleVendu.setDate_fin_encheres(rs.getDate("date_fin_encheres"));
                articleVendu.setPrix_initial(rs.getInt("prix_initial"));
                articleVendu.setPrix_vente(rs.getInt("prix_vente"));
                list.add(articleVendu);
            }
        } catch (SQLException e) {
            throw new DALException("selectByMotCle failed - " , e);
        } finally {
            try {
                if (rs != null){
                    rs.close();
                }
                if (rqt != null){
                    rqt.close();
                }
                if(cnx!=null){
                    cnx.close();
                }
            } catch (SQLException e) {
                throw new DALException("close failed " , e);
            }
        }
        return list;
    }

    @Override
    public List<Categorie> selectAllCategorie() throws DALException {
        Connection cnx = null;
        Statement rqt = null;
        ResultSet rs = null;
        List<Categorie> list = new ArrayList<Categorie>();
        try {
            cnx = JdbcTools.getConnection();
            rqt = cnx.createStatement();
            rs = rqt.executeQuery(sqlSelectAllCategorie);

            while (rs.next()) {

                Categorie categorie = new Categorie();
                categorie.setNo_categorie(rs.getInt("no_categorie"));
                categorie.setLibelle(rs.getString("libelle"));
                list.add(categorie);
            }
        } catch (SQLException e) {
            throw new DALException("selectAll failed - " , e);
        } finally {
            try {
                if (rs != null){
                    rs.close();
                }
                if (rqt != null){
                    rqt.close();
                }
                if(cnx!=null){
                    cnx.close();
                }
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return list;
    }
}
