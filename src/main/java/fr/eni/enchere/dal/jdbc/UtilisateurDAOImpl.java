package fr.eni.enchere.dal.jdbc;

import java.sql.*;

import fr.eni.enchere.bo.Utilisateur;
import fr.eni.enchere.dal.CreationUtilisateurDAO;

public class UtilisateurDAOImpl implements CreationUtilisateurDAO {

	private static final String INSERT_USER = "insert into Utilisateurs(pseudo,nom, prenom,email,telephone,rue,code_postal,ville,mot_de_passe,credit,administrateur) values(?,?,?,?,?,?,?,?,?,1,'false')";
	private static final String SELECT_USER = "select no_utilisateur,pseudo,nom,prenom,email,telephone,rue,code_postal,ville,mot_de_passe,credit,administrateur, etats from  Utilisateurs where pseudo = ? and mot_de_passe = ? ";
	private static final String SELECT_USER2 = "select no_utilisateur,pseudo,nom,prenom,email,telephone,rue,code_postal,ville,mot_de_passe,credit,administrateur, etats from  Utilisateurs where pseudo = ?";

	private static final String UPDATE_USER = "update UTILISATEURS set pseudo=?, nom=?, prenom=?, email=?, telephone=?, rue=?, code_postal=?, ville=?  from Utilisateurs where pseudo = ?";
	private static final String DELETE_USER = "update UTILISATEURS set pseudo=?, etats = 1 from UTILISATEURS where pseudo=?";

	public void insert(Utilisateur utilisateur) throws Exception {

		Connection cnx = null;
		try {
			cnx = JdbcTools.getConnection();
			PreparedStatement pstmt = cnx.prepareStatement(INSERT_USER, PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, utilisateur.getPseudo());
			pstmt.setString(2, utilisateur.getNom());
			pstmt.setString(3, utilisateur.getPrenom());
			pstmt.setString(4, utilisateur.getEmail());
			pstmt.setString(5, utilisateur.getTelephone());
			pstmt.setString(6,utilisateur.getRue());
			pstmt.setString(7,utilisateur.getCp());
			pstmt.setString(8,utilisateur.getVille());
			pstmt.setString(9, utilisateur.getMdp());
			//pstmt.setInt(10,utilisateur.getCredit());
			//pstmt.setBoolean(11,utilisateur.isAdministrateur());
			pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs.next()) {
				utilisateur.setNo_utilisateurs(rs.getInt(1));
			}
			rs.close();
			pstmt.close();


		} catch (Exception e) {
			throw e;
		} finally {
			if (cnx != null) {
				try {
					cnx.close();
				} catch (SQLException e) {
					e.printStackTrace();
					throw e;
				}
			}
		}

	}


	public void update(Utilisateur utilisateur) throws Exception {

		Connection cnx = null;
		try {
			cnx = JdbcTools.getConnection();
			PreparedStatement pstmt = cnx.prepareStatement(UPDATE_USER, PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, utilisateur.getPseudo());
			pstmt.setString(2, utilisateur.getNom());
			pstmt.setString(3, utilisateur.getPrenom());
			pstmt.setString(4, utilisateur.getEmail());
			pstmt.setString(5, utilisateur.getTelephone());
			pstmt.setString(6,utilisateur.getRue());
			pstmt.setString(7,utilisateur.getCp());
			pstmt.setString(8,utilisateur.getVille());
			pstmt.setString(9, utilisateur.getPseudo());
			//pstmt.setString(9, utilisateur.getMdp());
			//pstmt.setInt(10,utilisateur.getCredit());
			//pstmt.setBoolean(11,utilisateur.isAdministrateur());
			pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs.next()) {
				utilisateur.setNo_utilisateurs(rs.getInt(1));
			}
			rs.close();
			pstmt.close();

		} catch (Exception e) {
			throw e;
		} finally {
			if (cnx != null) {
				try {
					cnx.close();
				} catch (SQLException e) {
					e.printStackTrace();
					throw e;
				}
			}
		}

	}



	public Utilisateur select(String pseudo, String password, String etats) throws Exception {
		Utilisateur u = null;
		try (Connection cnx = JdbcTools.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_USER);
			pstmt.setString(1, pseudo);
			pstmt.setString(2, password);
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("no_utilisateur");
				//String pseudo = rs.getString("pseudo");
				String nom = rs.getString("nom");
				String prenom = rs.getString("prenom");
				String email = rs.getString("email");
				String telephone = rs.getString("telephone");
				String rue = rs.getString("rue");
				String cp = rs.getString("code_postal");
				String ville = rs.getString("ville");
				//int credit = rs.getInt("credit");
				//Boolean administrateur = rs.getBoolean("administrateur");


				u = new Utilisateur(id,pseudo,nom, prenom,email,telephone,rue,cp,ville,password,etats );
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return u;
	}








	public Utilisateur recupUtilisateur(String pseudo) throws Exception {


		try (Connection cnx = JdbcTools.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_USER2);
			pstmt.setString(1, pseudo);
			ResultSet rs = pstmt.executeQuery();

			if (rs.next()) {
				int id = rs.getInt("no_utilisateur");
				//String pseudo = rs.getString("pseudo");
				String nom = rs.getString("nom");
				String prenom = rs.getString("prenom");
				String email = rs.getString("email");
				String telephone = rs.getString("telephone");
				String rue = rs.getString("rue");
				String cp = rs.getString("code_postal");
				String ville = rs.getString("ville");
				//int credit = rs.getInt("credit");
				//Boolean administrateur = rs.getBoolean("administrateur");

				Utilisateur u = new Utilisateur(pseudo, nom, prenom, email, telephone, rue, cp, ville);
				return u;

			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;

		}
		return null;

	}

	public void delete(Utilisateur utilisateur) throws Exception {
		Connection cnx = null;
		try {
			cnx = JdbcTools.getConnection();
			PreparedStatement pstmt = cnx.prepareStatement(DELETE_USER, PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, utilisateur.getPseudo());
			pstmt.setString(2, utilisateur.getPseudo());

			pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs.next()) {
				utilisateur.setNo_utilisateurs(rs.getInt(1));
			}
			rs.close();
			pstmt.close();

		} catch (Exception e) {
			throw e;


		}
	}


}
