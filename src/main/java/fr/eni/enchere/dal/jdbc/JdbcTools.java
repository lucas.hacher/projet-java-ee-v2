package fr.eni.enchere.dal.jdbc;

import fr.eni.enchere.dal.Settings;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JdbcTools {
	private static String urlDB;
	private static String userDB;
	private static String pwdDB;

	static {
		urlDB = "jdbc:sqlserver://48SE36-2BLWH42;databaseName=bddEncheres";
		userDB = "sa";
		pwdDB = "Pa$$w0rd";
	}

	private static Connection con;

	public static Connection getConnection() throws SQLException {
		if (con == null || con.isClosed()) {
			con = DriverManager.getConnection(urlDB, userDB, pwdDB);
		}
		return con;
	}

	public static void close() throws SQLException {
		if (con != null) {
			con.close();
		}
	}
}
