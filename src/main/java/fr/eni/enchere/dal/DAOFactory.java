package fr.eni.enchere.dal;

import fr.eni.enchere.dal.jdbc.ArticleDAOImpl;
import fr.eni.enchere.dal.jdbc.UtilisateurDAOImpl;

public abstract class DAOFactory {
	
	public static CreationUtilisateurDAO getUtilisateurDAO()
	{
		return new UtilisateurDAOImpl();
	}

	public static ArticleDAO getArticleDAO()
	{
		return new ArticleDAOImpl();
	}


}
	