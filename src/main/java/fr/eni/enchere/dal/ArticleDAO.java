package fr.eni.enchere.dal;

import fr.eni.enchere.bo.ArticleVendu;
import fr.eni.enchere.bo.Categorie;

import java.util.List;

public interface ArticleDAO {

    //Sélectionner un article par son idArticle
    public ArticleVendu selectById(int id) throws DALException;

    //Sélectionner tous les articles
    public List<ArticleVendu> selectAllArticle() throws DALException;

    //Sélectionner les articles par categorie
    public List<ArticleVendu> selectByCategorie(String categorie) throws DALException;

    //Sélectionner les articles par mot clé
    //On recherche le mot clé dans le nom de l'article
    public List<ArticleVendu> selectByMotCle(String motCle) throws DALException;

    //Sélectionner les articles par mot clé et par catégorie
    public List<ArticleVendu> selectByMotCleAndByCategorie(String motCle, String categorie) throws DALException;

    public List<Categorie> selectAllCategorie() throws DALException;
}
