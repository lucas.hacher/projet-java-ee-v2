package fr.eni.enchere.dal;

import fr.eni.enchere.bo.Utilisateur;

public interface CreationUtilisateurDAO {
    void insert(Utilisateur utilisateur) throws Exception;

    Utilisateur select(String pseudo, String password, String etats) throws Exception;

    void update(Utilisateur utilisateur) throws Exception;

    public Utilisateur recupUtilisateur(String pseudo) throws Exception;

    void delete(Utilisateur utilisateur) throws Exception;


}
