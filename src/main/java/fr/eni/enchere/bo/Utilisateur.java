package fr.eni.enchere.bo;

public class Utilisateur {




    private int no_utilisateurs;
    private String pseudo;
    private String nom;
    private String prenom;
    private String email;
    private String telephone;
    private String rue;
    private String cp;
    private String ville;
    private String mdp;
    private int credit;
    private boolean administrateur;
    private String etats;

    public Utilisateur(int no_utilisateurs, String pseudo, String nom, String prenom, String email, String telephone, String rue, String cp, String ville, String mdp, int credit, boolean administrateur,String etats) {
        this.no_utilisateurs = no_utilisateurs;
        this.pseudo = pseudo;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.telephone = telephone;
        this.rue = rue;
        this.cp = cp;
        this.ville = ville;
        this.mdp = mdp;
        this.credit = credit;
        this.administrateur = administrateur;
        this.etats = etats;
    }

    public Utilisateur(String pseudo, String nom, String prenom, String email, String telephone, String rue, String cp, String ville, String mdp) {
        super();
        this.pseudo = pseudo;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.telephone = telephone;
        this.rue = rue;
        this.cp = cp;
        this.ville = ville;
        this.mdp = mdp;


    }

    public Utilisateur(int id, String pseudo, String nom, String prenom, String email, String telephone, String rue, String cp, String ville, String mdp) {
    }




    public Utilisateur(String pseudo, String nom, String prenom, String email, String telephone, String rue, String cp, String ville) {
        super();
        this.pseudo = pseudo;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.telephone = telephone;
        this.rue = rue;
        this.cp = cp;
        this.ville = ville;

    }




    public Utilisateur(String pseudo, String etats) {
        super();
        this.pseudo = pseudo;
        this.etats = etats;

    }

    public Utilisateur() {

    }


    public Utilisateur(int id, String pseudo, String nom, String prenom, String email, String telephone, String rue, String cp, String ville, String password, String etats) {
        super();
        this.pseudo = pseudo;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.telephone = telephone;
        this.rue = rue;
        this.cp = cp;
        this.ville = ville;
        this.etats = etats;
    }


    public int getNo_utilisateurs() {
        return no_utilisateurs;
    }

    public void setNo_utilisateurs(int no_utilisateurs) {
        this.no_utilisateurs = no_utilisateurs;
    }

    public String getPseudo() {
        return pseudo;
    }


    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getMdp() {
        return mdp;
    }


    public void setMdp(String mdp) {
        this.mdp = mdp;
    }


    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public boolean isAdministrateur() {
        return administrateur;
    }


    public void setAdministrateur(boolean administrateur) {
        this.administrateur = administrateur;
    }

    public String getEtats(){ return etats;}

    public void setEtats(String etats){this.etats = etats;}

    @Override
    public String toString() {
        return "Utilisateur{" +
                "no_utilisateurs=" + no_utilisateurs +
                ",pseudo=" + pseudo +
                ",nom=" + nom +
                ",prenom=" + prenom +
                ",email=" + email +
                ",telephone=" + telephone +
                ", rue=" + rue +
                ", cp=" + cp +
                ",ville =" + ville +
                ",mdp=" + mdp +
                ",credit=" + credit +
                ",administrateur" + administrateur +
                '}';


    }


}
