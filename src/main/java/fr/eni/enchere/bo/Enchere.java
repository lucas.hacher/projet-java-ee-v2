package fr.eni.enchere.bo;

import java.util.Date;

public class Enchere {



    private String numeroUtilisateurs;
    private String numeroArticles;
    private Date dateEncheres;
    private int montantEnchere;
    
    public Enchere(String numeroUtilisateurs, String numeroArticles, Date dateEncheres, int montantEnchere) {
        this.numeroUtilisateurs = numeroUtilisateurs;
        this.numeroArticles = numeroUtilisateurs;
        this.dateEncheres = dateEncheres;
        this.montantEnchere = montantEnchere;
    }

    public String getNumeroUtilisateurs() {
        return numeroUtilisateurs;
    }

    public void setNumeroUtilisateurs(String numeroUtilisateurs) {
        this.numeroUtilisateurs = numeroUtilisateurs;
    }

    public String getNumeroArticles() {
        return numeroArticles;
    }

    public void setNumeroArticles(String numeroArticles) {
        this.numeroArticles = numeroArticles;
    }

    public Date getDateEncheres() {
        return dateEncheres;
    }

    public void setDateEncheres(Date dateEncheres){
        this.dateEncheres = dateEncheres;
    }

    public int getMontantEnchere() {
        return montantEnchere;
    }

    public void setMontantEnchere(int montantEnchere){
        this.montantEnchere = montantEnchere;
    }

    @Override
    public String toString() {
        return "Enchere{" +
                "numeroUtilisateurs=" + numeroUtilisateurs +
                ", numeroArticles=" + numeroArticles +
                ", dateEncheres=" + dateEncheres +
                ",montantEnchere =" + montantEnchere +
                '}';
    
    
}
}


