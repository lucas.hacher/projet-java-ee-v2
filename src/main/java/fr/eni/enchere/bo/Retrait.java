package fr.eni.enchere.bo;


public class Retrait {



    private int no_articles;
    private String rue;
    private String cp;
    private String ville;

    public Retrait(int no_utilisateurs, String rue, String cp, String ville) {
        this.no_articles = no_utilisateurs;
        this.rue = rue;
        this.cp = cp;
        this.ville = ville;
    }

    public int getNo_utilisateurs() {
        return no_articles;
    }

    public void setNo_utilisateurs(int no_utilisateurs) {
        this.no_articles = no_utilisateurs;
    }


    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    @Override
    public String toString() {
        return "Retrait{" +
                "no_articles=" + no_articles +
                ", rue=" + rue +
                ", cp=" + cp +
                ",ville =" + ville +
                '}';


    }


}
