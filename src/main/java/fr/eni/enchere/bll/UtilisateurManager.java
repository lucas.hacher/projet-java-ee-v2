package fr.eni.enchere.bll;

import fr.eni.enchere.bo.ArticleVendu;
import fr.eni.enchere.bo.Utilisateur;
import fr.eni.enchere.dal.DAOFactory;
import fr.eni.enchere.dal.CreationUtilisateurDAO;

import javax.rmi.CORBA.Util;
import java.util.List;

public class UtilisateurManager {

    private CreationUtilisateurDAO creationUtilisateurDAO;

    // Pattern Singleton
    private static UtilisateurManager cUtilisateur;

    public static UtilisateurManager getCreationUtilisateur() {
        if (cUtilisateur == null) {
            cUtilisateur = new UtilisateurManager();
        }
        return cUtilisateur;
    }



    public Utilisateur recupUtilisateur(String pseudo) throws Exception {

        try {
            return creationUtilisateurDAO.recupUtilisateur(pseudo);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public UtilisateurManager() {
        creationUtilisateurDAO = DAOFactory.getUtilisateurDAO();
    }

    public void creerUnUtilisateur(String pseudo, String nom, String prenom, String email, String telephone, String rue,String cp,String ville,String mdp) throws Exception {
        // TODO Vérification des différents paramètres
        // Chiffrer le mot de passe
        //String hashPassword = MD5Utils.digest(password);

        Utilisateur u = new Utilisateur(pseudo,nom, prenom, email,telephone,rue,cp,ville,mdp);
        creationUtilisateurDAO.insert(u);
    }

    public void updateUnUtilisateur(String pseudo, String nom, String prenom, String email, String telephone, String rue,String cp,String ville)throws Exception {
        Utilisateur u = new Utilisateur(pseudo,nom, prenom, email,telephone,rue,cp,ville);
        creationUtilisateurDAO.update(u);
    }
    public void deleteUnUtilisateur(String pseudo, String etats)throws Exception {
        Utilisateur u = new Utilisateur(pseudo, etats);
        creationUtilisateurDAO.delete(u);
    }


    public Utilisateur verificationPseudoPassword(String pseudo, String mdp, String etats) {
        // TODO Vérification des différents paramètres
        // Chiffrer le mot de passe
        //String hashPassword = MD5Utils.digest(password);
        try {
            return creationUtilisateurDAO.select(pseudo, mdp, etats);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


}
