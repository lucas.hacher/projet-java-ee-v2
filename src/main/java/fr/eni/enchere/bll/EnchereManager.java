package fr.eni.enchere.bll;

import fr.eni.enchere.bo.ArticleVendu;
import fr.eni.enchere.bo.Categorie;
import fr.eni.enchere.bo.Utilisateur;
import fr.eni.enchere.dal.ArticleDAO;
import fr.eni.enchere.dal.CreationUtilisateurDAO;
import fr.eni.enchere.dal.DAOFactory;

import java.util.List;

public class EnchereManager {

    private ArticleDAO articleDAO;

    private static EnchereManager enchereManager;

    public static EnchereManager getEnchereManager() {
        if (enchereManager == null) {
            enchereManager = new EnchereManager();
        }
        return enchereManager;
    }

    public EnchereManager() {
        articleDAO = DAOFactory.getArticleDAO();
    }

    public ArticleVendu selectById(int id) throws Exception {

        try {
            return articleDAO.selectById(id);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<ArticleVendu> selectAllArticle() throws Exception {

        try {
            return articleDAO.selectAllArticle();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<ArticleVendu> selectByCategorie(String categorie) throws Exception {

        try {
            return articleDAO.selectByCategorie(categorie);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<ArticleVendu> selectByMotCle(String search) throws Exception {

        try {
            return articleDAO.selectByMotCle(search);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<ArticleVendu> selectByMotCleAndByCategorie(String search, String categorie) throws Exception {

        try {
            return articleDAO.selectByMotCleAndByCategorie(search, categorie);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Categorie> selectAllCategorie() throws Exception {

        try {
            return articleDAO.selectAllCategorie();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
