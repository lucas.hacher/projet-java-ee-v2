package fr.eni.enchere.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eni.enchere.bll.UtilisateurManager;
import fr.eni.enchere.bo.Utilisateur;
import fr.eni.enchere.dal.jdbc.JdbcTools;

/**
 * Servlet implementation class CreerUtilisateurServlet
 */
@WebServlet("/login")
public class ServletConnexion extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletConnexion() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Transfert de l'affichage à la JSP
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
        rd.forward(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Je lis les paramètres
        request.setCharacterEncoding("UTF-8");
        String pseudo = request.getParameter("pseudo");
        String password = request.getParameter("password");
        String etats = request.getParameter("etats");





        try {

            Utilisateur user = UtilisateurManager.getCreationUtilisateur().verificationPseudoPassword(pseudo, password,etats);
            String etat="select etats from  Utilisateurs where pseudo =?";

                if (user != null && !etat.equals("1")) {
                    HttpSession session = request.getSession();
                    session.setMaxInactiveInterval(5*60);


                    session.setAttribute("pseudo", pseudo);
                    session.setAttribute("password", password);

                    response.sendRedirect(request.getContextPath() + "/listeEnchere");
                } else {
                    // Sinon je retourne à la page d'ajout pour indiquer les problèmes:
                    request.setAttribute("erreurs", "Login/Password inconnu");
                    RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
                    rd.forward(request, response);
                }

        } catch (Exception e) {
            // Sinon je retourne à la page d'ajout pour indiquer les problèmes:
            e.printStackTrace();
            request.setAttribute("erreurs", e.getMessage());
            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
            rd.forward(request, response);
        }


    }

}
