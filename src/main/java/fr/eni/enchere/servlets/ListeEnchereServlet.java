package fr.eni.enchere.servlets;

import fr.eni.enchere.bll.EnchereManager;
import fr.eni.enchere.bo.ArticleVendu;
import fr.eni.enchere.bo.Categorie;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@WebServlet("/listeEnchere")
public class ListeEnchereServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute("pageTitle", "Liste des enchères");

        try {
            List<Categorie> listCategorie = EnchereManager.getEnchereManager().selectAllCategorie();

            if(listCategorie != null){
                request.setAttribute("categorieList", listCategorie);
                request.getRequestDispatcher( "/WEB-INF/jsp/listeEnchere.jsp" ).forward( request, response );
            } else {

                request.getRequestDispatcher( "/WEB-INF/jsp/listeEnchere.jsp" ).forward( request, response );
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {

            List<Categorie> listCategorie = EnchereManager.getEnchereManager().selectAllCategorie();
            String categorie = request.getParameter("categorieChoice");

            String search = request.getParameter("search");


            if(categorie == null && search.equals("")) {
                searchAndCategorieNull(request, response, listCategorie);

            } else if(categorie != null && search.equals("")) {
                searchNullAndCategorieTrue(request, response, listCategorie, categorie);

            } else if(categorie == null && !search.equals("")) {
                searchTrueAndCategorieNull(request, response, listCategorie, search);

            } else {
                searchTrueAndCategorieTrue(request, response, listCategorie, search, categorie);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void searchAndCategorieNull(HttpServletRequest request, HttpServletResponse response,
                                          List<Categorie> listCategorie) throws Exception {

        List<ArticleVendu> listArticle = EnchereManager.getEnchereManager().selectAllArticle();

        if(listArticle != null && listCategorie != null){
            request.setAttribute("articleList", listArticle);
            request.setAttribute("categorieList", listCategorie);
            request.getRequestDispatcher( "/WEB-INF/jsp/listeEnchere.jsp" ).forward( request, response );
        } else {
            request.getRequestDispatcher( "/WEB-INF/jsp/listeEnchere.jsp" ).forward( request, response );
        }
    }

    protected void searchNullAndCategorieTrue(HttpServletRequest request, HttpServletResponse response,
                                          List<Categorie> listCategorie, String categorie) throws Exception {

        List<ArticleVendu> listArticleByCategorie = EnchereManager.getEnchereManager().selectByCategorie(categorie);

        if(listArticleByCategorie != null){
            request.setAttribute("listArticleByCategorie", listArticleByCategorie);
            request.setAttribute("categorieList", listCategorie);
            request.getRequestDispatcher( "/WEB-INF/jsp/listeEnchere.jsp" ).forward( request, response );
        } else {
            request.getRequestDispatcher( "/WEB-INF/jsp/listeEnchere.jsp" ).forward( request, response );
        }
    }

    protected void searchTrueAndCategorieNull(HttpServletRequest request, HttpServletResponse response,
                                          List<Categorie> listCategorie, String search) throws Exception {

        List<ArticleVendu> listArticleByMotCle = EnchereManager.getEnchereManager().selectByMotCle(search);

        if(listArticleByMotCle != null){
            request.setAttribute("listArticleByMotCle", listArticleByMotCle);
            request.setAttribute("categorieList", listCategorie);
            request.getRequestDispatcher( "/WEB-INF/jsp/listeEnchere.jsp" ).forward( request, response );
        } else {
            request.getRequestDispatcher( "/WEB-INF/jsp/listeEnchere.jsp" ).forward( request, response );
        }
    }

    protected void searchTrueAndCategorieTrue(HttpServletRequest request, HttpServletResponse response,
                                              List<Categorie> listCategorie, String search, String categorie) throws Exception {

        List<ArticleVendu> listArticleByMotCleAndByCategorie =
                EnchereManager.getEnchereManager().selectByMotCleAndByCategorie(search, categorie);

        if(listArticleByMotCleAndByCategorie != null){
            request.setAttribute("listArticleByMotCleAndByCategorie", listArticleByMotCleAndByCategorie);
            request.setAttribute("categorieList", listCategorie);
            request.getRequestDispatcher( "/WEB-INF/jsp/listeEnchere.jsp" ).forward( request, response );
        } else {
            request.getRequestDispatcher( "/WEB-INF/jsp/listeEnchere.jsp" ).forward( request, response );
        }
    }
}
