package fr.eni.enchere.servlets;

import fr.eni.enchere.bll.EnchereManager;
import fr.eni.enchere.bll.UtilisateurManager;
import fr.eni.enchere.bo.Utilisateur;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/profil")
public class ServletUpdate extends  HttpServlet {
    private static final long serialVersionUID = 1L;


    public ServletUpdate() {

        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        request.setAttribute("viewFile", "profil.jsp");
        request.setAttribute("pageTitle", "Profil");
        /*try {
            request.setAttribute("articleList", EnchereManager.getEnchereManager().selectAll());
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        HttpSession session = request.getSession();

        String pseudo = (String) session.getAttribute("pseudo");
        try {
            request.setAttribute("utilisateur", UtilisateurManager.getCreationUtilisateur().recupUtilisateur(pseudo));
        } catch (Exception e) {
            e.printStackTrace();
        }
        request.getRequestDispatcher( "/WEB-INF/jsp/profil.jsp" ).forward( request, response );
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter pw = response.getWriter();
        String valid = request.getParameter("valid");

        // Lecture paramètre
        request.setCharacterEncoding("UTF-8");
        String pseudo = request.getParameter("pseudo");
        String nom = request.getParameter("nom");
        String prenom = request.getParameter("prenom");
        String email = request.getParameter("email");
        String telephone = request.getParameter("telephone");
        String rue = request.getParameter("rue");
        String cp = request.getParameter("cp");
        String ville = request.getParameter("ville");
        //String password = request.getParameter("mdp");
        //int credit = Integer.getInteger(request.getParameter("credit"));
        //Boolean administrateur = Boolean.valueOf(request.getParameter("admnistrateur"));
        String etats = request.getParameter("etats");

        try{
        if ("update".equals(valid)) {
            UtilisateurManager.getCreationUtilisateur().updateUnUtilisateur(pseudo, nom, prenom, email, telephone, rue, cp, ville);
            response.sendRedirect(request.getContextPath() + "/profil");
        }else if("delete".equals(valid)){
            UtilisateurManager.getCreationUtilisateur().deleteUnUtilisateur(pseudo, etats);
            request.getSession().invalidate();
            response.sendRedirect(request.getContextPath() + "/listeEnchere");
        }

        }catch(Exception e ){
            // Sinon je retourne à la page d'ajout pour indiquer les problèmes:
            e.printStackTrace();
            request.setAttribute("erreurs", e.getMessage());
            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/jsp/inscription.jsp");
            rd.forward(request, response);
            }


    }
}
