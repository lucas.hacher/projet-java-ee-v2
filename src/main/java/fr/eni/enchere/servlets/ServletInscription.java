package fr.eni.enchere.servlets;

import fr.eni.enchere.bll.UtilisateurManager;
import fr.eni.enchere.dal.jdbc.JdbcTools;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/inscription")
public class ServletInscription extends  HttpServlet {
    private static final long serialVersionUID = 1L;


    public ServletInscription() {

        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/jsp/inscription.jsp");
        rd.forward(request, response);

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Lecture paramètre
        request.setCharacterEncoding("UTF-8");
        String pseudo = request.getParameter("pseudo");
        String nom = request.getParameter("nom");
        String prenom = request.getParameter("prenom");
        String email = request.getParameter("email");
        String telephone = request.getParameter("telephone");
        String rue = request.getParameter("rue");
        String cp = request.getParameter("cp");
        String ville = request.getParameter("ville");
        String password = request.getParameter("mdp");
        //int credit = Integer.getInteger(request.getParameter("credit"));
        //Boolean administrateur = Boolean.valueOf(request.getParameter("admnistrateur"));

        try {
            Connection cnx = JdbcTools.getConnection();
            String Query="select no_utilisateur,pseudo,nom,prenom,email,telephone,rue,code_postal,ville,mot_de_passe,credit,administrateur from  Utilisateurs where email =?";
            String Query2="select no_utilisateur,pseudo,nom,prenom,email,telephone,rue,code_postal,ville,mot_de_passe,credit,administrateur from  Utilisateurs where pseudo =?";
            PreparedStatement psmt=cnx.prepareStatement(Query);
            PreparedStatement psmt2=cnx.prepareStatement(Query2);
            psmt.setString(1, email);
            psmt2.setString(1, pseudo);
            ResultSet rs=psmt.executeQuery();
            ResultSet rs2=psmt2.executeQuery();
            if(rs.next())
            {
                response.sendRedirect("inscription");


            }

            else if(rs2.next()) {
                response.sendRedirect("inscription");
            }

            else {

                UtilisateurManager.getCreationUtilisateur().creerUnUtilisateur(pseudo, nom, prenom, email, telephone, rue, cp, ville, password);

                response.sendRedirect(request.getContextPath() + "/login");
            }
        } catch (Exception e) {
            // Sinon je retourne à la page d'ajout pour indiquer les problèmes:
            e.printStackTrace();
            request.setAttribute("erreurs", e.getMessage());
            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/jsp/inscription.jsp");
            rd.forward(request, response);
        }

    }
}



