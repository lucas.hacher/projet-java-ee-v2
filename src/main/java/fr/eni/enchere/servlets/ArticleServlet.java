package fr.eni.enchere.servlets;

import fr.eni.enchere.bll.EnchereManager;
import fr.eni.enchere.bo.ArticleVendu;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/ref")
public class ArticleServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute("pageTitle", "Article");
        try {
            String idString = request.getParameter("idArticle");
            int id = Integer.parseInt(idString);

            ArticleVendu article = EnchereManager.getEnchereManager().selectById(id);

            request.setAttribute("article", article);
            request.getRequestDispatcher( "/WEB-INF/jsp/article.jsp" ).forward( request, response );

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
